function trocaTemp(Fahrenheit) {
    var celsius = ((Fahrenheit - 32) / 9) * 5
    return celsius.toFixed(2)
}


var numeroRandom = (Math.random() * 100).toFixed(2);

temp = trocaTemp(numeroRandom)

    console.log('A temperatura ${numeroRandom} em F°, sendo ${temp} C°.')